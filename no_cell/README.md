# neuron.jl

This repository contains Julia code for flexibly constructing a neuron and adding channels. 

First, add `neuron.jl` to your workspace:

```julia
include("neuron.jl")
```

Create an empty neuron with no channels:

```julia
AB = Cell()
```

Add a voltage gated Sodium channel with density 183 mS/cm^2:

```
add!(AB,NaV(183.))
```

Similarly, you can add a bunch of other channels:

```
add!(AB,CaT(2.3))
add!(AB,CaS(2.7))
add!(AB,A(24.6))
add!(AB,KCa(98.))
add!(AB,Kd(61.))
add!(AB,H(1.))
add!(AB,Leak(.099))
```

The order you add channels in does not matter. You can defined your own channels: see `conductances.jl` for an example. Briefly, your own channel is a mutable struct that inherits from abstract type `Conductance` and implements the following methods with these type signatures:

```
function integrate(channel::YourChannel,V::Float64,Ca::Float64,dt::Float64)
    # do whatever you want here 
end

function effectiveConductance(channel::YourChannel)
    # do something, and return the effective conductance 
    # (a Float64)
end
```

That's all `Cell` cares about. 

You can specify initial conditions of the cell using:

```
AB.V = 0; #mV
AB.Ca = 0.05; # uM
```

To specify other initial conditions, for example of various channels you added, you can drill in:

```
AB.conductance[1].m = 0.1;
```

To integrate all equations for one time step of size `dt`, use:

```
dt = 50e-3;
integrate(AB,dt)
```

To integrate all equations for many time steps till some end point, use:


```
dt = 50e-3;
t_end = 1e3;
V,Ca = integrate(AB,dt,t_end);
```



