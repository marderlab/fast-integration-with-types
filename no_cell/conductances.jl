# conductances.jl
# defines types and functions for various conductances

abstract type Conductance
    # all conductances now inherit from this
end

# create a container for simulation parameters
immutable SimParams
    area::Float64       # mm^2
    Cm::Float64         # nF/mm^2
    extCal::Float64     # μM
    τCal::Float64       # ms
    Cal0::Float64       # μM
    factor::Float64     # μM/nA
    RTzF::Float64       # mV, to compute the calcium reversal potential
    dt::Float64         # ms
    tmax::Float64       # ms
    CalCurrents::Array{Int64,1}

    # void constructor method
    SimParams() = new(0.0628,10.0,3000,200,0.05,14.96,12.20008405,50e-3,5000.0,[2,3])
end

# create a container for neurons
mutable struct Cell
    V::Float64          # mV
    Cal::Float64        # μM
end

# helper functions
boltz(V::Float64,A::Float64,B::Float64) = 1/(1 + exp((V+A)/B))
tauX(V::Float64,A::Float64,B::Float64,D::Float64,E::Float64) = A - B/(1+exp((V+D)/E))

# leak conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Leak <: Conductance
    g::Float64      # maximal conductance in μS/mm^2
    E::Float64

    # constructor
    Leak(x) = new(x,-50.0)
end

function integrate(channel::Leak,cell::Cell,dt::Float64)
    # do nothing
end

function effectiveConductance(channel::Leak)
    return channel.g;
end

# fast sodium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct NaV <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    NaV(x::Float64)  = new(x,0.,0.,30.)
end

function integrate(channel::NaV,cell::Cell,dt::Float64)
    minf = boltz(cell.V,25.5,-5.29)
    hinf = boltz(cell.V,48.9,5.18)
    tauh = (0.67/(1+exp((cell.V+62.9)/-10.0)))*(1.5 + 1/(1+exp((cell.V+34.9)/3.6)))

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/tauX(cell.V,1.32,1.26,120.,-25.));
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);
end

function effectiveConductance(channel::NaV)
    return channel.g*(channel.m^3)*channel.h;
end

# transient calcium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct CaT <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    CaT(x::Float64)  = new(x,0.0,0.0,NaN)
end

function integrate(channel::CaT,cell::Cell,dt::Float64)
    minf::Float64 = boltz(cell.V,27.1,-7.2)
    hinf::Float64 = boltz(cell.V,32.1,5.5)

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/tauX(cell.V,21.7,21.3,68.1,-20.5));
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauX(cell.V,105.,89.8,55.,-16.9));
end

function effectiveConductance(channel::CaT)
    return channel.g*(channel.m^3)*channel.h;
end


# slow calcium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct CaS <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    CaS(x::Float64)  = new(x,0.,0.,NaN)
end

function integrate(channel::CaS,cell::Cell,dt::Float64)
    minf::Float64 = boltz(cell.V,33.,-8.1)
    hinf::Float64 = boltz(cell.V,60.,6.2)
    taum::Float64 = 1.4 + (7/((exp((cell.V+27)/10))+(exp((cell.V+70)/-13))))
    tauh::Float64 = 60 + (150/((exp((cell.V+55)/9))+(exp((cell.V+65)/-16))))

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/taum);
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);
end

function effectiveConductance(channel::CaS)
    return channel.g*(channel.m^3)*channel.h;
end

# A current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct A <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    A(x::Float64)  = new(x,0.,0.,-80.)
end

function integrate(channel::A,cell::Cell,dt::Float64)
    hinf::Float64 = boltz(cell.V,56.9,4.9)
    minf::Float64 = boltz(cell.V,27.2,-8.7)

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(cell.V,11.6,10.4,32.9,-15.2));
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauX(cell.V,38.6,29.2,38.9,-26.5));
end

function effectiveConductance(channel::A)
    return channel.g*(channel.m^3)*channel.h;
end

# KCa current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct KCa <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    KCa(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::KCa,cell::Cell,dt::Float64)
    minf::Float64 = (cell.Cal/(cell.Cal+3))*(1/(1+exp((cell.V+28.3)/-12.6)));

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(cell.V,90.3,75.1,46.,-22.7));
end

function effectiveConductance(channel::KCa)
    return channel.g*(channel.m^4);
end

# Kd current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Kd <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    Kd(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::Kd,cell::Cell,dt::Float64)
    minf::Float64 = boltz(cell.V,12.3,-11.8);

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(cell.V,7.2,6.4,28.3,-19.2));
end

function effectiveConductance(channel::Kd)
    return channel.g*(channel.m^4);
end

# H current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct H <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    H(x::Float64)  = new(x,0.,-20.)
end

function integrate(channel::H,cell::Cell,dt::Float64)
    minf::Float64 = boltz(cell.V,70.0,6.0);

    channel.m = minf + (channel.m - minf)*exp(-dt/((272.0 + 1499.0/(1.0+exp((cell.V + 42.2)/-8.73)))));
end

function effectiveConductance(channel::H)
    return channel.g*channel.m;
end
