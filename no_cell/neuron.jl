# neuron.jl
# specifies types and function for a single compartment neuron
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~ begin function definitions ~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# step forward once in the integration procedure
function integrate(V::Float64, Cal::Float64, con::Array{Conductance,1}, sim::SimParams)

    # create placeholder variables
    g           = Array{Float64,1}(length(con))
    ∑g::Float64 = 0;
    ∑gE::Float64 = 0;
    ###
    # compute calcium reversal potentials
    E_Ca = sim.RTzF * log(sim.extCal / Cal);
    # if a channel is a Calcium channel, set the reversal potential
    @inbounds for ii in sim.CalCurrents
        con[ii].E   = E_Ca
    end

    # integrate ionic gating variables for every channel type
    @inbounds for ii = 1:length(con)
        integrate(con[ii], cell::Cell, sim.dt)
        # compute effective conductance
        g[ii]       = effectiveConductance(con[ii]);
        # update the placeholder variables
        ∑g          += g[ii];
        ∑gE         += g[ii] * con[ii].E;
    end
    ###
    # compute Calcium currents
    Ca_current::Float64 = 0.0
    @inbounds for ii in sim.CalCurrents
        Ca_current  += effectiveConductance(con[ii])
    end
    # set the Calcium current
    Ca_current      = Ca_current * (V - E_Ca)

    # compute V_inf, tau_V using g <---- m(t), h(t)
    V∞::Float64     = ∑gE/∑g; # V_inf in mV
    τV::Float64     = sim.Cm/(∑g*10);
    # integrate V
    V = V∞ + (V - V∞)*exp(-(sim.dt/τV));                       # mV

    # compute Cal∞
    Cal∞::Float64   = sim.Cal0 - sim.factor*sim.area*Ca_current;         # μM
    # integrate Cal
    ###
    Cal        = Cal∞ + (Cal - Cal∞)*exp(-sim.dt/sim.τCal);   # μM

    return V, Cal
end

# integrate for t_end ms using timestep dt (in ms)
function simulate(V::Float64,Cal::Float64,con::Array{Conductance,1},sim::SimParams)
    # pre-allocate arrays
    nsteps  = Int64(sim.tmax/sim.dt);
    Vvec       = Array{Float64,1}(nsteps);
    Calvec     = Array{Float64,1}(nsteps)
    @inbounds for ii = 2:nsteps
        Vvec[ii], Calvec[ii] = integrate(Vvec[ii-1],Calvec[ii-1],con,sim);
    end
    return Vvec, Calvec
end
