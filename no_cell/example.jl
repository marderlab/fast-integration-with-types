workspace(); gc();
include("conductances.jl")
include("neuron.jl")

using Plots; plotlyjs();

function neurogenesis()
    cell = Cell(-70.0,0.05)
    sim = SimParams()
    con = Array{Conductance,1}()
    push!(con,NaV(183.))
    push!(con,CaT(2.3))
    push!(con,CaS(2.7))
    push!(con,A(24.6))
    push!(con,KCa(98.))
    push!(con,Kd(61.))
    push!(con,H(1.))
    push!(con,Leak(.099))
    return cell, con, sim
end

# perform simulation
cell, con, sim = neurogenesis();
V = -70.0; Cal = 0.05;
@time V, Cal = simulate(V,Cal,con,sim)
plot(Cal,
    xlabel="# of steps",
    ylabel="membrane potential (mV)",
    grid=false,
    legend=false)
gui()
