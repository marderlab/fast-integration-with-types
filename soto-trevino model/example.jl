workspace(); gc();

include("neuron.jl")
# replicated from Soto-Trevino 2005

## Create AB neuron

# soma/neurite compartment
ABSN        = Cell()
ABSN.Cm     = 9.0   # nF
ABSN.τCal   = 303.  # ms
ABSN.f      = 0.418 # μM/nA
ABSN.Cal0   = 0.5   # μM
ABSN.extCal = 13000.# μM

add!(ABSN,CaTAB(55.2))
add!(ABSN,CaS(9.))
add!(ABSN,NaP(2.7))
add!(ABSN,H(0.054))
add!(ABSN,Kd(1890.))
add!(ABSN,KCaAB(6000.))
add!(ABSN,AAB(200.0))
add!(ABSN,MI(570.))
add!(ABSN,Leak(0.045))

# axonal compartment
ABAX        = Cell()
ABAX.Cm     = 1.5   # nF
ABAX.τCal   = 303.  # ms
ABAX.f      = 0.418 # μM/nA
ABAX.Cal0   = 0.5   # μM
ABAX.extCal = 13000.# μM

add!(ABAX,NaV(300.))
add!(ABAX,Kd(52.5))
add!(ABAX,Leak(0.0018))
ABAX.E[3]   = -60.0

## Create PD neuron

# soma/neurite compartment
PDSN        = Cell()
PDSN.Cm     = 12.   # nF
PDSN.τCal   = 300.  # ms
PDSN.f      = 0.515 # μM/nA
PDSN.Cal0   = 0.5   # μM
PDSN.extCal = 13000.# μM

add!(PDSN,CaTPD(22.5))
add!(PDSN,CaS(60.))
add!(PDSN,NaP(4.38))
add!(PDSN,H(0.219))
add!(PDSN,Kd(1576.8))
add!(PDSN,KCaPD(251.85))
add!(PDSN,APD(39.42))
add!(PDSN,Leak(0.105))

# axonal compartment
PDAX        = Cell()
PDAX.Cm     = 6.0   # nF
PDAX.τCal   = 300.  # ms
PDAX.f      = 0.515 # μM/nA
PDAX.Cal0   = 0.5   # μM
PDAX.extCal = 13000.# μM

add!(PDAX,NaV(1100.))
add!(PDAX,Kd(150.))
add!(PDAX,Leak(0.00081))
PDAX.E[3]   = -55.0

## Create Network and Synapses
network     = [ABSN, ABAX, PDSN, PDAX]
synapses    = Array{Synapse,1}()
add!(synapses,Gap(0.75,1,3))    # AB to PD
add!(synapses,Gap(0.75,3,1))    # PD to AB
add!(synapses,Gap(0.3,1,2))     # ABSN to ABAX
add!(synapses,Gap(0.3,2,1))     # ABAX to ABSN
add!(synapses,Gap(1.05,3,4))    # PDSN to PDAX
add!(synapses,Gap(1.05,4,3))    # PDAX to PDSN

## Simulation parameters
dt = 50e-3; tmax = 5e3;

ABSN.V      = -70.0
ABSN.Cal    = 0.5
ABAX.V      = -70.0
ABAX.Cal    = 0.5
PDSN.V      = -70.0
PDSN.Cal    = 0.5
PDAX.V      = -70.0
PDAX.Cal    = 0.5

V,Cal = integrate(network,synapses,dt,tmax)

##
network = [ABSN];
synapses = Array{Synapse,1}()
dt = 50e-3
tmax = 5e3
using Plots; plotlyjs();
plot(V); gui();
