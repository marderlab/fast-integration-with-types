# conductances.jl
# defines types and functions for various conductances


abstract type Conductance
    # all conductances now inherit from this
end

# helper functions
boltz(V::Float64,A::Float64,B::Float64) = 1/(1 + exp((V+A)/B))
tauX(V::Float64,A::Float64,B::Float64,D::Float64,E::Float64) = A - B/(1+exp((V+D)/E))



# leak conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Leak <: Conductance
    g::Float64      # maximal conductance in μS/mm^2
    E::Float64

    # constructor
    Leak(x) = new(x,-50.0);
end

function integrate(channel::Leak,V::Float64,Ca::Float64,dt::Float64)
    return Void()
end

function getConductance(channel::Leak)
    channel.g
end


# fast sodium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct NaV <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    NaV(x::Float64)  = new(x,0.,0.,50.)
end

function integrate(channel::NaV,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,24.7,-5.29);
    hinf = boltz(V,48.9,5.18);
    tauh = (0.67/(1+exp((V+62.9)/-10.0)))*(1.5 + 1/(1+exp((V+34.9)/3.6)));

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,1.32,1.26,120.,-25.));
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);
    return Void()
end

function getConductance(channel::NaV)
    channel.g*(channel.m^3)*channel.h;
end

# transient calcium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct CaTAB <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    CaTAB(x::Float64)  = new(x,0.0,0.0,130.)
end

function integrate(channel::CaTAB,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,25.,-7.2);
    hinf = boltz(V,36.,7.);

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,55.,49.5,58.,17.));
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauX(V,87.5,75.,50.,-16.9));
    return Void()
end

function getConductance(channel::CaTAB)
    channel.g*(channel.m^3)*channel.h;
end

mutable struct CaTPD <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    CaTPD(x::Float64)  = new(x,0.0,0.0,130.)
end

function integrate(channel::CaTPD,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,25.,-7.2);
    hinf = boltz(V,36.,7.);

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,55.,49.5,58.,17.));
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauX(V,350.,300.,50.,-16.9));
    return Void()
end

function getConductance(channel::CaTPD)
    channel.g*(channel.m^3)*channel.h;
end

# slow calcium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct CaS <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    CaS(x::Float64)  = new(x,0.,130.)
end

function integrate(channel::CaS,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,22.,-8.5);

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,16.,13.1,25.1,-26.4));

    return Void()
end

function getConductance(channel::CaS)
    channel.g*(channel.m^3);
end

# NaP current ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct NaP <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    NaP(x::Float64) = new(x,0.,0.,50.)
end

function integrate(channel::NaP,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,25.8,-8.2)
    hinf = boltz(V,48.5,4.8)

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,19.8,10.7,26.5,-8.6))
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauX(V,666.,379.,33.6,11.7))

    return Void()
end

function getConductance(channel::NaP)
    channel.g * (channel.m^3) * channel.h
end

# H current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct H <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    H(x::Float64)  = new(x,0.,-20.)
end

function integrate(channel::H,V::Float64,Ca::Float64,dt::Float64)
    channel.m = boltz(V,70.0,6.0) + (channel.m - boltz(V,70.0,6.0))*exp(-dt/((272.0 + 1499.0/(1.0+exp((V + 42.2)/-8.73)))));

    return Void()
end

function getConductance(channel::H)
    channel.g*channel.m;
end

# Kd current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Kd <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    Kd(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::Kd,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,14.2,-11.8);

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,7.2,6.4,28.3,-19.2));

    return Void()
end

function getConductance(channel::Kd)
    channel.g*(channel.m^4);
end

# KCa current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct KCaAB <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    KCaAB(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::KCaAB,V::Float64,Ca::Float64,dt::Float64)
    minf = (Ca/(Ca+30))*(1/(1+exp((V+51.)/-4.)));

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,90.3,75.1,46.,-22.7));

    return Void()
end

function getConductance(channel::KCaAB)
    channel.g*(channel.m^4);
end

mutable struct KCaPD <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    KCaPD(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::KCaPD,V::Float64,Ca::Float64,dt::Float64)
    minf = (Ca/(Ca+30))*(1/(1+exp((V+51.)/-8.)));

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,90.3,75.1,46.,-22.7));

    return Void()
end

function getConductance(channel::KCaPD)
    channel.g*(channel.m^4);
end

# A current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct AAB <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    AAB(x::Float64)  = new(x,0.,0.,-80.)
end

function integrate(channel::AAB,V::Float64,Ca::Float64,dt::Float64)
    channel.m = boltz(V,27.,-8.7) + (channel.m - boltz(V,27.,-8.7))*exp(-dt/tauX(V,11.6,10.4,32.9,-15.2));
    @fastmath channel.h = boltz(V,56.9,4.9) + (channel.h - boltz(V,56.9,4.9))*exp(-dt/tauX(V,38.6,29.2,38.9,-26.5));

     return Void()
end

function getConductance(channel::AAB)
    channel.g*(channel.m^3)*channel.h;
end

mutable struct APD <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    APD(x::Float64)  = new(x,0.,0.,-80.)
end

function integrate(channel::APD,V::Float64,Ca::Float64,dt::Float64)
    channel.m = boltz(V,27.,-8.7) + (channel.m - boltz(V,27.,-8.7))*exp(-dt/tauX(V,11.6,10.4,32.9,-15.2));
    @fastmath channel.h = boltz(V,56.9,4.9) + (channel.h - boltz(V,56.9,4.9))*exp(-dt/tauX(V,38.6,29.2,38.9,-26.5));

     return Void()
end

function getConductance(channel::APD)
    channel.g*(channel.m^4)*channel.h;
end

# Modulatory Input (proctolin mimic) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct MI <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    MI(x::Float64) = new(x,0.,0.)
end

function integrate(channel::MI,V::Float64,Ca::Float64,dt::Float64)
    channel.m = boltz(V,12.,-3.05) + (channel.m - boltz(V,12.,-3.05))*exp(-dt/0.5)

    return Void()
end

function getConductance(channel::MI)
    channel.g * channel.m
end

## Synapses
abstract type Synapse end

# Electric synapse (gap junction) ~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Gap <: Synapse
    g::Float64  # μS
    n_pre::Int64
    n_post::Int64

    Gap(x::Float64,y::Int64,z::Int64) = new(x,y,z)
end

function integrate(syn::Gap,Vpost::Float64,Vpre::Float64,dt::Float64)
    return Void()
end

function getConductance(syn::Gap,Vpost::Float64,Vpre::Float64)
    syn.g * (Vpost - Vpre)   # nA
end

# Chemical synapse (glutamatergic, graded) ~~~~~~~~~~~~~~
mutable struct Glut <: Synapse
    g::Float64
    s::Float64
    E::Float64

    n_pre::Int64
    n_post::Int64

    # constructor
    Glut(x::Float64,y::Int64,z::Int64) = new(x,0.,-70.0,y,z)
end

function integrate(syn::Glut,Vpost::Float64,Vpre::Float64,dt::Float64)
    sinf    = boltz(Vpre,-35.,-5.)
    syn.s   = sinf + (syn.s - sinf)*exp(-dt/(40.*(1-sinf)))
    return Void()
end

function getConductance(syn::Glut)
    syn.s * (Vpost - syn.E)
end

# Chemical synapse (cholinergic, graded) ~~~~~~~~~~~~~~~~
mutable struct Chol <: Synapse
    g::Float64
    s::Float64
    E::Float64

    n_pre::Int64
    n_post::Int64

    # constructor
    Chol(x::Float64) = new(x,0.,-80.0)
end

function integrate(syn::Chol,Vpost::Float64,Vpre::Float64,dt::Float64)
    sinf    = boltz(Vpre,-35.,-5.)
    syn.s   = sinf + (syn.s - sinf)*exp(-dt/(100.*(1-sinf)))
    return Void()
end

function getConductance(syn::Chol)
    syn.s * (Vpost - syn.E)
end
