workspace(); gc();

include("neuron.jl")
using Plots; pyplot()

function makePacemaker()
    cells       = Array{Cell,1}()
    synapses    = Array{Synapse,1}()

    AB          = Cell()
    add!(AB,NaV(1830.))     # ḡ in μS/mm^2
    add!(AB,CaT(23.))
    add!(AB,CaS(27.))
    add!(AB,A(246.))
    add!(AB,KCa(980.))
    add!(AB,Kd(610.))
    add!(AB,H(10.))
    add!(AB,Leak(.99))

    PD          = Cell()
    add!(PD,NaV(1830.))
    add!(PD,CaT(23.))
    add!(PD,CaS(27.))
    add!(PD,A(246.))
    add!(PD,KCa(980.))
    add!(PD,Kd(610.))
    add!(PD,H(10.))
    add!(PD,Leak(.99))

    add!(synapses,Gap(1e-3,1,2))
    add!(synapses,Gap(1e-3,2,1))

    AB.V    = -70.0
    PD.V    = -70.0
    AB.Cal  = 0.05
    PD.Cal  = 0.05

    push!(cells,AB)
    push!(cells,PD)
    println("created AB and PD cells in a network")
    return cells, synapses
end

# simulation parameters
dt = 50e-3; tmax = 5e3;

# create network
cells, synapses = makePacemaker();

# integrate network
V,Cal = integrate(cells,synapses,dt,tmax)

# plot
plot(V,layout=(2,1))
