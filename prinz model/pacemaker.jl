workspace(); gc();

include("neuron.jl")
using Plots; pyplot()

function makePacemaker()
    cells       = Array{Cell,1}()
    synapses    = Array{Synapse,1}()

    AB          = Cell()
    add!(AB,NaV(1830.))     # ḡ in μS/mm^2
    add!(AB,CaT(23.))
    add!(AB,CaS(27.))
    add!(AB,A(246.))
    add!(AB,KCa(980.))
    add!(AB,Kd(610.))
    add!(AB,H(10.))
    add!(AB,Leak(.99))

    PD          = Cell()
    add!(PD,NaV(1830.))
    add!(PD,CaT(23.))
    add!(PD,CaS(27.))
    add!(PD,A(246.))
    add!(PD,KCa(980.))
    add!(PD,Kd(610.))
    add!(PD,H(10.))
    add!(PD,Leak(.99))

    add!(synapses,Gap(1e-3,1,2))
    add!(synapses,Gap(1e-3,2,1))

    AB.V    = -70.0
    PD.V    = -70.0
    AB.Cal  = 0.05
    PD.Cal  = 0.05

    push!(cells,AB)
    push!(cells,PD)
    println("created AB and PD cells in a network")
    return cells, synapses
end

# simulation parameters
dt = 50e-3; tmax = 5e3;

# create network
cells, synapses = makePacemaker();


# increasing coupling strength
V               = zeros(Int64(tmax/dt),10)
[synapses[ii].g = 0.0 for ii in 1:length(synapses)]
for ii in 1:2:10
    cells, synapses = makePacemaker();
    [synapses[qq].g = 2*(ii-1) for qq in 1:length(synapses)]
    V[:,ii:ii+1], Cal = integrate(cells,synapses,dt,tmax)
end

using LaTeXStrings
plot(linspace(dt,tmax,Int64(tmax/dt)),V,layout=(10,1),grid=false,legend=false,
    xlabel  = ["" "time (ms)"],
    ylabel  = "mV",
    size    = (600,1200),
    ylim    = (-70,30))

# decreasing PD calcium

# decreasing CaT
cells, synapses = makePacemaker();
V               = zeros(Int64(tmax/dt),10)
[synapses[ii].g = 5.0 for ii in 1:length(synapses)]
for ii in 1:2:10
    cells, synapses = makePacemaker();
    cells[2].conductance[2].g = 25.0 - 2*ii
    V[:,ii:ii+1], Cal = integrate(cells,synapses,dt,tmax)
end
plot(linspace(dt,tmax,Int64(tmax/dt)),V,layout=(10,1),grid=false,legend=false,
    xlabel  = ["" "time (ms)"],
    ylabel  = "mV",
    size    = (600,1200))
gui()

# decreasing CaS
cells, synapses = makePacemaker();
V               = zeros(Int64(tmax/dt),10)
[synapses[ii].g = 5.0 for ii in 1:length(synapses)]
for ii in 1:2:10
    cells, synapses = makePacemaker();
    cells[2].conductance[3].g = 27.0 - 2*ii
    V[:,ii:ii+1], Cal = integrate(cells,synapses,dt,tmax)
end
plot(linspace(dt,tmax,Int64(tmax/dt)),V,layout=(10,1),grid=false,legend=false,
    xlabel  = ["" "time (ms)"],
    ylabel  = "mV",
    size    = (600,1200))
gui()

# decreasing CaS and CaT
cells, synapses = makePacemaker();
V               = zeros(Int64(tmax/dt),10)
[synapses[ii].g = 5.0 for ii in 1:length(synapses)]
for ii in 1:2:10
    cells, synapses = makePacemaker();
    cells[2].conductance[2].g = 25.0 - ii
    cells[2].conductance[3].g = 27.0 - ii
    V[:,ii:ii+1], Cal = integrate(cells,synapses,dt,tmax)
end
plot(linspace(dt,tmax,Int64(tmax/dt)),V,layout=(10,1),grid=false,legend=false,
    xlabel  = ["" "time (ms)"],
    ylabel  = "mV",
    size    = (600,1200))
gui()

## Making an Ersatz AB cell
function makeErsatzAB()
    ABSN    = Cell()
    add!(ABSN,NaV(1830.))
    add!(ABSN,CaT(23.))
    add!(ABSN,CaS(27.))
    add!(ABSN,A(246.))
    add!(ABSN,KCa(980.))
    add!(ABSN,Kd(610.))
    add!(ABSN,H(10.))
    add!(ABSN,Leak(.99))

    ABAX    = Cell()
    add!(ABAX,Leak(0.99))

    cells       = Array{Cell,1}()
    synapses    = Array{Synapse,1}()

    push!(cells,ABSN)
    push!(cells,ABAX)

    add!(synapses,Gap(1.0e-3,1,2))
    add!(synapses,Gap(1.0e-3,2,1))

    ABSN.V      = -70.0
    ABAX.V      = -70.0
    ABSN.Cal    = 0.05
    ABAX.Cal    = 0.05
    return cells, synapses
end

# simulate
dt          = 50e-3
tmax        = 5e3
t           = linspace(dt,tmax,Int64(tmax/dt))

scale       = 1.0e-3

## Leak Current in ABAX
V           = zeros(length(t),2)
cells, synapses = makeErsatzAB()
V,Cal       = integrate(cells,synapses,dt,tmax)

# loop through increasing leak strength
V               = zeros(Int64(tmax/dt),10)
cells, synapses = makeErsatzAB()
[synapses[ii].g = 0.0 for ii in 1:length(synapses)]
for ii in 1:2:10
    cells, synapses = makeErsatzAB()
    synapses[1].g   = (ii-1)*scale
    synapses[2].g   = (ii-1)*scale
    V[:,ii:ii+1], Cal = integrate(cells,synapses,dt,tmax)
end
plot(t,V,
    layout=(10,1),grid=false,legend=false,
    xlabel  = ["" "time (ms)"],
    ylabel  = "mV",
    ylims   = (-70.0,30.0))
gui()

## Adding Additional Currents
V               = zeros(Int64(tmax/dt),10)
cells, synapses = makeErsatzAB()
[synapses[ii].g = 1.0e-2 for ii in 1:length(synapses)]
add!(cells[2],NaV(1830.))
add!(cells[2],Kd(610.))
cells[1].conductance[1].g = 1830.
cells[2].Cm     = 1.5   # Soto-Trevino
V, Cal = integrate(cells,synapses,dt,tmax)
plot(t,V,
    layout=(2,1),grid=false,legend=false,
    xlabel  = ["" "time (ms)"],
    ylabel  = "mV",
    ylims   = (-70.0,30.0))
gui()
