include("neuron.jl")
using Plots; plotlyjs()

function makePrinz()
    cells       = Array{Cell,1}()
    synapses    = Array{Synapse,1}()

    # AB #2
    AB          = Cell()
    add!(AB,NaV(1000.0))
    add!(AB,CaT(25.))
    add!(AB,CaS(60.0))
    add!(AB,A(500.0))
    add!(AB,KCa(50.0))
    add!(AB,Kd(1000.0))
    add!(AB,H(0.1))
    add!(AB,Leak(0.00))

    push!(cells,AB)

    # LP #4
    LP          = Cell()
    add!(LP,NaV(1000.0))
    add!(LP,CaT(0.0))
    add!(LP,CaS(40.0))
    add!(LP,A(200.0))
    add!(LP,KCa(0.0))
    add!(LP,Kd(250.0))
    add!(LP,H(0.5))
    add!(LP,Leak(0.3))

    push!(cells,LP)

    # PY #1
    PY          = Cell()
    add!(PY,NaV(1000.0))
    add!(PY,CaT(25.))
    add!(PY,CaS(20.))
    add!(PY,A(500.0))
    add!(PY,KCa(0.0))
    add!(PY,Kd(1250.0))
    add!(PY,H(0.5))
    add!(PY,Leak(0.1))

    push!(cells,PY)

    # synapses
    # AB = 1, LP = 2, PY = 3
    add!(synapses,Glut(0.,1,2))

    # LP synapses
    add!(synapses,Glut(30.,2,1))
    add!(synapses,Chol(30.,2,1))
    add!(synapses,Glut(30.,2,3))

    # PY synapses
    add!(synapses,Glut(10.,3,1))
    add!(synapses,Chol(3.,3,1))
    add!(synapses,Glut(0.03,3,2))

    println("created AB, LP, and PY cells in a network")
    return cells, synapses
end

# simulate
dt  = 50e-3; tmax = 10e3;

cells, synapses = makePrinz()
#synapses = Array{Synapse,1}()

V,Cal = integrate(cells,synapses,dt,tmax)

plot(V,layout=(3,1),ylims=(-70,30))

## test a four-neuron circuit
begin
    cells       = Array{Cell,1}()
    synapses    = Array{Synapse,1}()

    # AB #2
    AB          = Cell()
    add!(AB,NaV(1000.0))
    add!(AB,CaT(25.))
    add!(AB,CaS(60.0))
    add!(AB,A(500.0))
    add!(AB,KCa(50.0))
    add!(AB,Kd(1000.0))
    add!(AB,H(0.1))
    add!(AB,Leak(0.00))

    push!(cells,AB)

    PD          = Cell()
    add!(PD,NaV(1000.0))
    add!(PD,CaT(25.))
    add!(PD,CaS(60.0))
    add!(PD,A(500.0))
    add!(PD,KCa(50.0))
    add!(PD,Kd(1000.0))
    add!(PD,H(0.1))
    add!(PD,Leak(0.00))

    push!(cells,PD)
    # LP #4
    LP          = Cell()
    add!(LP,NaV(1000.0))
    add!(LP,CaT(0.0))
    add!(LP,CaS(40.0))
    add!(LP,A(200.0))
    add!(LP,KCa(0.0))
    add!(LP,Kd(250.0))
    add!(LP,H(0.5))
    add!(LP,Leak(0.3))

    push!(cells,LP)

    # PY #1
    PY          = Cell()
    add!(PY,NaV(1000.0))
    add!(PY,CaT(25.))
    add!(PY,CaS(20.))
    add!(PY,A(500.0))
    add!(PY,KCa(0.0))
    add!(PY,Kd(1250.0))
    add!(PY,H(0.5))
    add!(PY,Leak(0.1))

    push!(cells,PY)

    # synapses
    # AB = 1, PD = 2, LP = 3, PY = 4
    add!(synapses,Glut(1.,2,3))
    add!(synapses,Gap(10.,1,2))
    add!(synapses,Gap(10.,2,1))

    # LP synapses
    add!(synapses,Glut(30.,3,1))
    add!(synapses,Chol(30.,3,2))
    add!(synapses,Glut(30.,3,3))

    # PY synapses
    add!(synapses,Glut(10.,4,1))
    add!(synapses,Chol(3.,4,2))
    add!(synapses,Glut(0.3,4,3))
end

V,Cal = integrate(cells,synapses,dt,tmax)
plot(V,layout=(4,1),ylims = (-70,30))

a =1

## Test an AB Neuron

# make network
cells       = Array{Cell,1}()
synapses    = Array{Synapse,1}()

# make cell
cell          = Cell()
add!(cell,NaV(1000.0))
add!(cell,CaT(25.))
add!(cell,CaS(60.0))
add!(cell,A(500.0))
add!(cell,KCa(50.0))
add!(cell,Kd(1000.0))
add!(cell,H(0.1))
add!(cell,Leak(0.00))

push!(cells,cell)

# perform simulation
V,Cal = integrate(cells,synapses,dt,tmax)
plot(V,ylims=(-70,30))

## Test an LP Neuron

# make network
cells       = Array{Cell,1}()
synapses    = Array{Synapse,1}()

# make cell
cell          = Cell()
add!(cell,NaV(1000.0))
add!(cell,CaT(0.0))
add!(cell,CaS(40.0))
add!(cell,A(200.0))
add!(cell,KCa(0.0))
add!(cell,Kd(250.0))
add!(cell,H(0.5))
add!(cell,Leak(0.3))

push!(cells,cell)

# perform simulation
V,Cal = integrate(cells,synapses,dt,tmax)
plot(V,ylims=(-70,30))

## Test a PY Neuron

# make network
cells       = Array{Cell,1}()
synapses    = Array{Synapse,1}()

# make cell
cell          = Cell()
add!(cell,NaV(1000.0))
add!(cell,CaT(25.))
add!(cell,CaS(20.))
add!(cell,A(500.0))
add!(cell,KCa(0.0))
add!(cell,Kd(1250.0))
add!(cell,H(0.5))
add!(cell,Leak(0.1))

push!(cells,cell)

# perform simulation
V,Cal = integrate(cells,synapses,dt,tmax)
plot(V,ylims=(-70,30))

## Half-Center Oscillator
dt  = 50e-3; tmax = 5e3;

# make network
cells       = Array{Cell,1}()
synapses    = Array{Synapse,1}()

# make a cell
cell          = Cell()
add!(cell,NaV(1000.0))
add!(cell,CaT(0.0))
add!(cell,CaS(40.0))
add!(cell,A(200.0))
add!(cell,KCa(0.0))
add!(cell,Kd(250.0))
add!(cell,H(0.5))
add!(cell,Leak(0.3))
cell2          = Cell()
cell2.V = 0.0
add!(cell2,NaV(1000.0))
add!(cell2,CaT(0.0))
add!(cell2,CaS(40.0))
add!(cell2,A(200.0))
add!(cell2,KCa(0.0))
add!(cell2,Kd(250.0))
add!(cell2,H(0.5))
add!(cell2,Leak(0.3))

push!(cells,cell)
push!(cells,cell2)
add!(synapses,Glut(0.1,2,1))
add!(synapses,Glut(0.1,1,2))

# plot
V,Cal = integrate(cells,synapses,dt,tmax)
plot(V,ylims=(-70,30))
