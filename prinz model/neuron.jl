# neuron.jl
# specifies types and function for a single compartment neuron

include("conductances.jl")


# create a container for neurons
mutable struct Cell
    # dynamic variables
    V::Float64                              # mV
    Cal::Float64                            # μM

    # conductances
    conductance::Array{Conductance,1}

    # parameters
    area::Float64       # mm^2
    Cm::Float64         # nF/mm^2
    extCal::Float64     # μM
    τCal::Float64       # ms
    Cal0::Float64       # μM
    f::Float64          # μM/nA
    RT_by_zF::Float64   # to compute E_Cal

    # housekeeping
    Cal_channels::Array{Int64,1} # marker for which channels are calcium channels
    E::Array{Float64,1}

    # constructor
    Cell() = new(-70.0,0.05,Array{Conductance,1}(),0.0628,10,3000,200,0.05,14.96,12.20008405,Array{Int64,1}(),Array{Float64,1}())
end

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~ begin function definitions ~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add cell to cells
function add!(cells::Array{Cell,1},cell::Cell)
    push!(cells,cell)
end

function add!(synapses::Array{Synapse,1},syn::Synapse)
    push!(synapses,syn)
end

# add conductance to cell
function add!(cell::Cell, g::Conductance)
    if length(string(typeof(g))) > 1
        if string(typeof(g))[1:2] == "Ca"
            push!(cell.Cal_channels,length(cell.conductance)+1)
        end
    end
    push!(cell.conductance,g)
    push!(cell.E,g.E)
end

function integrate(cells::Array{Cell,1},synapses::Array{Synapse,1},dt::Float64)
    Vinf    = Array{Float64,1}(length(cells))
    tauV    = Array{Float64,1}(length(cells))
    Vvec    = Array{Float64,1}(length(cells))
    Calvec  = Array{Float64,1}(length(cells))
    Ivec    = Array{Float64,1}(length(cells))

    @inbounds for ii in 1:length(cells)
        cell = cells[ii]
        V = cell.V;
        Cal = cell.Cal;

        # integrate m, h for every channel and
        @inbounds for i = 1:length(cell.conductance)
            integrate(cell.conductance[i], V, Cal, dt)
        end

        # integrate the synapses
        for j in 1:length(synapses)
            if synapses[j].n_post == ii
                integrate(synapses[j],V,cells[synapses[j].n_pre].V,dt)
            end
        end
    end

    @inbounds for ii in 1:length(cells)
        cell = cells[ii]
        V = cell.V;
        Cal = cell.Cal;
        g = Array{Float64,1}(length(cell.conductance))
        sigma_gE = 0;
        Isyn::Float64 = 0.0

        @inbounds for qq in 1:length(cell.conductance)
            g[qq] = getConductance(cell.conductance[qq])
            sigma_gE += g[qq]*cell.E[qq]
        end

        sigma_g = sum(g);

        # synapses
        for j in 1:length(synapses)
            if synapses[j].n_post == ii
                Isyn += getConductance(synapses[j],V,cells[synapses[j].n_pre].V)
            end
        end
        # compute Calcium currents
        E_Cal = cell.RT_by_zF*log(cell.extCal/Cal);
        cell.E[cell.Cal_channels] = E_Cal;
        Cal_current = (V - E_Cal)*sum(g[cell.Cal_channels]);

        # compute V_inf, tau_V using g <---- m(t), h(t)
        V_inf::Float64 = (sigma_gE+Isyn/cell.area)/sigma_g; # V_inf in mV
        tau_V::Float64 = cell.Cm/(sigma_g);

        # integrate V
        cell.V = V_inf + (V - V_inf)*exp(-(dt/tau_V)); # mV

        # compute Cal_inf
        cinf::Float64 = cell.Cal0 - cell.f*cell.area*Cal_current; # microM
        cell.Cal = cinf + (Cal - cinf)*exp(-dt/cell.τCal); # microM
        Vvec[ii]    = cell.V
        Calvec[ii]  = cell.Cal
    end
    return Vvec, Calvec
end

function integrate(cells::Array{Cell,1},synapses::Array{Synapse,1},dt::Float64,tmax::Float64)
    nsteps  = Int64(tmax/dt)
    V       = zeros(nsteps,length(cells))
    Cal     = zeros(nsteps,length(cells))

    @inbounds for ii in 1:nsteps
        V[ii,:], Cal[ii,:] = integrate(cells, synapses, dt)
    end
    return V, Cal
end
