# Fast Integration with Types

Well, it's not so fast, but it will have to do for now.

The core files are `conductances.jl` and `neuron.jl`. `conductances.jl` contains
the definitions for the ionic and synaptic conductances and the `integrate()`
functions for them. The `neuron.jl` code contains the functions to add cells to
networks, add synapses to synapse lists, and to add conductances to cells.


The core function is `integrate(network, synapses, dt, tmax)` which returns an
`Array{Float64,2}` variable for both membrane potential and intracellular calcium.
The rows are the values and the columns are the cells. 

## Sample Code

```julia
include("neuron.jl")
function getPacemaker()
    network     = Array{Cell,1}()
    synapses    = Array{Synapse,1}()

    AB          = Cell()
    add!(AB,NaV(183.))
    add!(AB,CaT(2.3))
    add!(AB,CaS(2.7))
    add!(AB,A(24.6))
    add!(AB,KCa(98.))
    add!(AB,Kd(61.))
    add!(AB,H(1.))
    add!(AB,Leak(.099))

    PD          = Cell()
    add!(PD,Leak(.099))

    add!(synapses,Gap(1e-3))
    add!(synapses,Gap(1e-3))

    add!(network,AB)
    add!(network,PD)

    AB.V    = -70.0
    PD.V    = -70.0
    AB.Cal  = 0.05
    PD.Cal  = 0.05
    println("created AB and PD cells in a network")
    return network, synapses
end

dt = 50e-3; tmax = 5e3;
post_syn    = [1,2]
pre_syn     = [2,1]
syndex      = [1,2]
network, synapses = getPacemaker();
V,Cal = integrate(network,synapses,dt,tmax)

##
using Plots; plotlyjs();
plot(V,layout=2); gui();
```