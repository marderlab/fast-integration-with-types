# produces a vector of spike-times
function getSpikes(volt::Array{Float64,1})
  spikes    = zeros(size(volt))
  for ii in 1:length(volt)-1
    if volt[ii] <0 && volt[ii+1] > 0
      spikes[ii] = 1
    end
  end
  return spikes
end

# produces a matrix of spike-times
function getSpikes(volt::Array{Float64,2})
  spikes = zeros(size(volt))
  for ii in 1:size(volt,1)-1
    for qq in 1:size(volt,2)
      if volt[ii,qq] < 0 && volt[ii+1,qq] > 0
        spikes[ii,qq] = 1
      end
    end
  end
  return spikes
end

# produces a vector of burst-times
function getBursts(volt::Array{Float64,1}; tol::Int64 = 100, dt::Float64 = 50e-3)
  # tol and dt are in milliseconds
  tolerance   = convert(Int64,tol/dt)
  # get the linear indices of the spike-times
  spk_time    = find(getSpikes(volt))
  # interspike intervals
  spk_diff    = push!(diff(spk_time),0.0)
  bursts      = zeros(length(volt))
  for ii in 1:length(spk_time)-1
    if spk_diff[ii] >= tolerance
      bursts[spk_time[ii+1]] = 1
    end
  end
  return bursts
end

# produces a matrix of burst-times
function getBursts(volt::Array{Float64,2}; tol=100, dt = 50e-3)
  # tol is in milliseconds
  tolerance = convert(Int64,tol/dt)
  # get the linear indices of spike-times in an array of arrays
  spk_time  = [find(getSpikes(volt[:,ii])) for ii in 1:length(volt[1,:])]
  # gets the interspike intervals in an array of arrays
  spk_diff  = [push!([diff(spk_time[ii]) for ii in 1:length(spk_time)][qq],0.0)
              for qq in 1:length(spk_time)]
  bursts    = zeros(size(volt))
  for ii in 1:length(spk_time)
    for qq in 1:length(spk_time[ii])-1
      if spk_diff[ii][qq] >= tolerance
        # interburst intervals in a 2-D array
        bursts[spk_time[ii][qq+1],ii] = 1
      end
    end
  end
  return bursts
end

# filters the data by taking the mean of fixed bins
function meanfilter(input::Array{Float64,1}, Nbins::Int64 = 100)
  output           = zeros(length(input))
  scale::Int64     = convert(Int64,length(input)/Nbins)
  for ii in 1:Nbins-1
    start::Int64   = 1 + scale*(ii-1)
    stop::Int64    = scale*(ii+1)
    output[start:stop] = scale*mean(input[start:stop])
  end
  return output
end

# filters the data by taking the mean of fixed bins
function meanfilter(input::Array{Float64,2},Nbins::Int64 = 100)
  output             = zeros(size(input))
  scale::Int64       = convert(Int64,length(input[:,1])/Nbins)
  for ii in 1:length(input[1,:])
    for qq in 1:Nbins-1
      start::Int64   = 1 + scale*(qq-1)
      stop::Int64    = scale*(qq+1)
      output[start:stop,ii]   = scale*mean(input[start:stop,ii])
    end
  end
  return output
end

# filters the data and then gets a vector of burst-times
function getBursts(input::Array{Float64,1}; Nbins::Int64 = 100, tol::Int64 = 100, bst_tol::Int64 = 4)
  signal1   = meanfilter(getSpikes(input),Nbins)
  signal2   = getBursts(input,tol)
  bst_time  = find(signal2)
  output    = zeros(length(input))
  for ii in 1:length(bst_time)
    if signal1[bst_time[ii]] >= bst_tol
      output[bst_time[ii]] = 1
    end
  end
  return output
end

# filters the data and then gets a matrix of burst-times
function getBursts(input::Array{Float64,2}; Nbins::Int64 = 100, tol::Int64 = 100, bst_tol::Int64 = 4)
  NBINS = copy(Nbins); TOL = copy(tol); BST_TOL = copy(bst_tol)
  signal1   = meanfilter(getSpikes(input),tol)  # returns a matrix
  signal2   = getBursts(input,Nbins = NBINS, tol = TOL, bst_tol = BST_TOL)                # returns a matrix
  bst_time  = [find(getBursts(input[:,ii])) for ii in 1:length(input[1,:])]
  output    = zeros(size(input))
  for ii in 1:length(bst_time)
    for qq in 1:length(bst_time[ii])
      if signal1[bst_time[ii][qq],ii] >= bst_tol
        output[bst_time[ii][qq],ii] = 1
      end
    end
  end
  return output
end
################################################################


## Assorted Junk


# produces a matrix of burst-times from filtered data
function getBursts(input::Array{Float64,2},Nbins::Int64,tol::Int64,bst_tol::Int64)
  signal1   = meanfilter(getSpikes(input),Nbins)  # returns a matrix
  signal2   = getBursts(input,tol)                # returns a matrix
  bst_time  = [find(getBursts(input[:,ii],tol)) for ii in 1:length(input[1,:])]
  output    = zeros(size(input))
  for ii in 1:length(bst_time)
    for qq in 1:length(bst_time[ii])
      if signal1[bst_time[ii][qq],ii] >= bst_tol
        output[bst_time[ii][qq],ii] = 1
      end
    end
  end
  return output
end

# gets the interburst interval as a Float
function getIBI(volt::Array{Float64,1},tol)
  bst_time  = find(getBursts(volt,tol))
  bst_diff  = push!(diff(bst_time),0.0)
  # take the mean of the last 4 interburst intervals
  IBI       = dt*(mean(bst_diff[end-4:end-1]))
  return IBI  # ms
end
# gets the interburst intervals as a vector
function getIBI(volt::Array{Float64,2},tol::Int64,burst_steps::Int64)
  # gets the times of each burst in an array of arrays
  bst_time  = [find(getBursts(volt[:,ii],tol)) for ii in 1:length(volt[1,:])]
  # gets the interburst intervals in an array of arrays
  bst_diff  = [push!([diff(bst_time[ii]) for ii in 1:length(bst_time)][qq],0.0)
              for qq in 1:length(bst_time)]
  IBI       = zeros(length(volt[1,:]))
  for ii in 1:length(IBI)
    # take the mean of the last 4 interburst intervals
    IBI[ii] = dt*mean(bst_diff[ii][end-burst_steps:end-1])
  end
  return IBI  # ms
end
# gets the interburst interval as a Float using the filtered data
function getIBI(input::Array{Float64,1},Nbins::Int64,tol::Int64,bst_tol::Int64)
  bursts    = getBursts(input,Nbins,tol,bst_tol)
  start     = convert(Int64,length(input)*0.25)
  IBI       = dt*mean(diff(find(bursts[start:end])))
  return IBI
end
# gets the interburst interval as a vector using the filtered data
function getIBI(input::Array{Float64,2},Nbins::Int64,tol::Int64,bst_tol::Int64)
  bursts    = [getBursts(input[:,ii],Nbins,tol,bst_tol) for ii in 1:size(input,2)]
  start     = round(Int64,size(input,1)*0.25)
  IBI       = [dt*mean(diff(find(bursts[ii][start:end]))) for ii in 1:size(input,2)]
  return IBI
end
