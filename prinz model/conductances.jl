# conductances.jl
# defines types and functions for various conductances


abstract type Conductance
    # all conductances now inherit from this
end

# helper functions
boltz(V::Float64,A::Float64,B::Float64) = 1/(1 + exp((V+A)/B))
tauX(V::Float64,A::Float64,B::Float64,D::Float64,E::Float64) = A + B/(1+exp((V+D)/E))



# leak conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Leak <: Conductance
    g::Float64      # maximal conductance in μS/mm^2
    E::Float64

    # constructor
    Leak(x) = new(x,-50.0);
end

function integrate(channel::Leak,V::Float64,Ca::Float64,dt::Float64)
    return Void()
end

function getConductance(channel::Leak)
    return channel.g
end

# fast sodium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct NaV <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    NaV(x::Float64)  = new(x,0.,0.,50.)
end

function integrate(channel::NaV,V::Float64,Ca::Float64,dt::Float64)
    minf = 1.0/(1.0+exp((V+25.5)/-5.29));
    hinf = 1.0/(1.0+exp((V+48.9)/5.18));
    taum = 2.64 - 2.52/(1+exp((V+120.0)/-25.0))
    tauh = (1.34/(1.0+exp((V+62.9)/-10.0)))*(1.5+1.0/(1.0+exp((V+34.9)/3.6)));

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/taum);
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);
    return Void()
end

function getConductance(channel::NaV)
    return channel.g*(channel.m^3)*channel.h;
end

# transient calcium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct CaT <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    CaT(x::Float64)  = new(x,0.0,0.0,130.)
end

function integrate(channel::CaT,V::Float64,Ca::Float64,dt::Float64)
    minf = 1.0/(1.0 + exp((V+27.1)/-7.2))
    hinf = 1.0/(1.0 + exp((V+32.1)/5.5))
    taum = 43.4 - 42.6/(1.0 + exp((V+68.1)/-20.5))
    tauh = 210.0 - 179.6/(1.0 + exp((V+55.0)/-16.9))

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/taum);
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);
    return Void()
end

function getConductance(channel::CaT)
    channel.g*(channel.m^3)*channel.h;
end


# slow calcium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct CaS <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    CaS(x::Float64)  = new(x,0.,0.,130.)
end

function integrate(channel::CaS,V::Float64,Ca::Float64,dt::Float64)
    minf = 1.0/(1.0+exp((V+33.0)/-8.1))
    hinf = 1.0/(1.0+exp((V+60.0)/6.2))
    taum = 2.8 + 14.0/(exp((V+27.0)/10.0) + exp((V+70.0)/-13.0))
    tauh = 120.0 + 300.0/(exp((V+55.0)/9.0) + exp((V+65.0)/-16.0))

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/taum);
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);

    return Void()
end

function getConductance(channel::CaS)
    channel.g*(channel.m^3)*channel.h;
end

# A current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct A <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    A(x::Float64)  = new(x,0.,0.,-80.)
end

function integrate(channel::A,V::Float64,Ca::Float64,dt::Float64)
    minf = 1.0/(1.0+exp((V+27.2)/-8.7))
    hinf = 1.0/(1.0+exp((V+56.9)/4.9))
    taum = 23.2 - 20.8/(1.0+exp((V+32.9)/-15.2))
    tauh = 77.2 - 58.4/(1.0+exp((V+38.9)/-26.5))

    @fastmath channel.m = minf + (channel.m - minf)*exp(-dt/taum);
    @fastmath channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);

     return Void()
end

function getConductance(channel::A)
    channel.g*(channel.m^3)*channel.h;
end

# KCa current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct KCa <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    KCa(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::KCa,V::Float64,Ca::Float64,dt::Float64)
    minf = (Ca/(Ca+3.0))/(1.0+exp((V+28.3)/-12.6))
    taum = 180.6 - 150.2/(1.0+exp((V+46.0)/-22.7))

    channel.m = minf + (channel.m - minf)*exp(-dt/taum);

    return Void()
end

function getConductance(channel::KCa)
    channel.g*(channel.m^4);
end


# Kd current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Kd <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    Kd(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::Kd,V::Float64,Ca::Float64,dt::Float64)
    minf = 1.0/(1.0+exp((V+12.3)/-11.8))
    taum = 14.4 - 12.8/(1.0+exp((V+28.3)/-19.2))

    channel.m = minf + (channel.m - minf)*exp(-dt/taum);

    return Void()
end

function getConductance(channel::Kd)
    channel.g*(channel.m^4);
end

# H current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct H <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    H(x::Float64)  = new(x,0.,-20.)
end

function integrate(channel::H,V::Float64,Ca::Float64,dt::Float64)
    minf = 1.0/(1.0+exp((V+75.0)/5.5))
    taum = 2/( exp((V+169.7)/(-11.6)) + exp((V- 26.7)/(14.3)) )

    channel.m = minf + (channel.m - minf)*exp(-dt/taum);

    return Void()
end

function getConductance(channel::H)
    channel.g*channel.m;
end

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Synapses ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

abstract type Synapse end

# Electric synapse (gap junction) ~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Gap <: Synapse
    g::Float64  # μS
    n_pre::Int64
    n_post::Int64

    Gap(x::Float64,y::Int64,z::Int64) = new(x,y,z)
end

function integrate(syn::Gap,Vpost::Float64,Vpre::Float64,dt::Float64)
    return Void
end

function getConductance(syn::Gap,Vpost::Float64,Vpre::Float64)
    syn.g * (Vpost - Vpre)   # nA
end

# Chemical synapse (glutamatergic, graded) ~~~~~~~~~~~~~~
mutable struct Glut <: Synapse
    g::Float64
    s::Float64
    E::Float64

    n_post::Int64
    n_pre::Int64

    # constructor
    Glut(x::Float64,y::Int64,z::Int64) = new(x,0.,-70.0,y,z)
end

function integrate(syn::Glut,Vpost::Float64,Vpre::Float64,dt::Float64)
    sinf    = 1.0/(1.0+exp((-35.0 - Vpre)/5.0))
    taus    = (1.0 - sinf)/0.025
    syn.s   = sinf + (syn.s - sinf)*exp(-dt/taus)
    return Void()
end

function getConductance(syn::Glut,Vpost::Float64,Vpre::Float64)
    syn.g * syn.s * (Vpost - syn.E) / 1000
end

# Chemical synapse (cholinergic, graded) ~~~~~~~~~~~~~~~~
mutable struct Chol <: Synapse
    g::Float64
    s::Float64
    E::Float64

    n_post::Int64
    n_pre::Int64

    # constructor
    Chol(x::Float64,y::Int64,z::Int64) = new(x,0.,-80.0,y,z)
end

function integrate(syn::Chol,Vpost::Float64,Vpre::Float64,dt::Float64)
    sinf    = 1.0/(1.0+exp((-35.0 - Vpre)/5.0))
    taus    = (1.0 - sinf)/0.01
    syn.s   = sinf + (syn.s - sinf)*exp(-dt/taus)
    return Void()
end

function getConductance(syn::Chol,Vpost::Float64,Vpre::Float64)
    syn.g * syn.s * (Vpost - syn.E) / 1000
end
